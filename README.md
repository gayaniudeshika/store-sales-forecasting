## Sales Prediction Web App

This is a Flask web application for predicting sales using machine learning. The application utilizes Celery for background task processing and includes a gradient boosting regressor model trained on historical sales data.

## Prerequisites

- Python 3
- Flask
- Celery
- pandas
- NumPy
- scikit-learn
- joblib

## Installation

1. Clone this repository to your local machine.
2. Install the required dependencies using pip:

    ```bash
    pip install -r requirements.txt
    ```

## Usage

1. Ensure that you have the necessary CSV files in the `dataset` directory: `train.csv`, `test.csv`, `transactions.csv`, `stores.csv`, `holidays_events.csv`, and `oil.csv`.
2. Run the Flask application:

    ```bash
    python app.py
    ```

3. Access the application in your web browser at `http://localhost:5000`.

## Application Structure

- `app.py`: Main Flask application file containing routes and model training tasks.
- `templates/`: Directory containing HTML templates for rendering web pages.
- `static/`: Directory containing static files such as images and CSS.
- `dataset/`: Directory containing CSV files for training and testing data.

## How to Predict Sales

1. Visit the home page at `http://localhost:5000/`.
2. Enter the required information and submit the form to predict sales.
3. View the prediction on the result page.

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details.

---