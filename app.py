from celery import Celery
from flask import Flask, render_template, request, redirect, url_for
import pandas as pd
import numpy as np
from sklearn.ensemble import GradientBoostingRegressor
import joblib

app = Flask(__name__)
celery = Celery(app.name, broker='amqp://guest:guest@localhost//')

# Load the data only once during initialization
train_data = pd.read_csv("dataset/train.csv")
test_data = pd.read_csv("dataset/test.csv")
transactions_data = pd.read_csv("dataset/transactions.csv")
stores_data = pd.read_csv("dataset/stores.csv")
holidays_events_data = pd.read_csv("dataset/holidays_events.csv")
oil_data = pd.read_csv("dataset/oil.csv")

# Define preprocess_data function
def preprocess_data(train, test, transactions, stores, oil, holidays_events):
    # Data preprocessing
    train = pd.merge(train, stores, how='left', on='store_nbr')
    train = pd.merge(train, transactions, how='left', on=['date', 'store_nbr'])
    train = pd.merge(train, oil, how='left', on='date')
    train = pd.merge(train, holidays_events, how='left', on='date')

    test = pd.merge(test, stores, how='left', on='store_nbr')
    test = pd.merge(test, transactions, how='left', on=['date', 'store_nbr'])
    test = pd.merge(test, oil, how='left', on='date')
    test = pd.merge(test, holidays_events, how='left', on='date')

    # Convert 'date' column to datetime type
    train['date'] = pd.to_datetime(train['date'])
    test['date'] = pd.to_datetime(test['date'])

    # Feature engineering - Extract day of week
    train['day_of_week'] = train['date'].dt.dayofweek
    test['day_of_week'] = test['date'].dt.dayofweek

    # Feature engineering - Add lag features
    def create_lag_features(df, lag_days=7):
        lag_cols = [f'sales_lag_{lag}' for lag in range(1, lag_days+1)]
        if 'sales' in df.columns:
            for col in lag_cols:
                df[col] = df.groupby('store_nbr')['sales'].shift(1)
        else:
            for col in lag_cols:
                df[col] = 0  # Fill with zeros if 'sales' column is missing
        return df

    train = create_lag_features(train)
    test = create_lag_features(test)

    # Log transformation for sales
    train['log_sales'] = np.log1p(train['sales'])

    # Feature selection
    features = ['store_nbr', 'day_of_week', 'cluster', 'dcoilwtico']
    for lag in range(1, 8):
        features.append(f'sales_lag_{lag}')
    features.append('onpromotion')

    # One-hot encode 'onpromotion'
    train['onpromotion'] = train['onpromotion'].astype(int)
    test['onpromotion'] = test['onpromotion'].astype(int)

    # Splitting the data into features (X) and target variable (y)
    X_train = train[features].fillna(0)
    y_train = train['log_sales']
    X_test = test[features].fillna(0)

    return X_train, y_train, X_test


# Train model task (Celery)
@celery.task
def train_model_periodically():
    # Train the model periodically (e.g., every day)
    X_train, y_train, X_test = preprocess_data(train_data, test_data, transactions_data, stores_data, oil_data, holidays_events_data)
    model = GradientBoostingRegressor()
    model.fit(X_train, y_train)
    joblib.dump(model, 'trained_model.pkl')


# Home page
@app.route('/')
def index():
    header_image_url = url_for('static', filename='header.png')
    return render_template('index.html', header_image_url=header_image_url)

# Prediction route
@app.route('/predict', methods=['POST'])
def predict():
    if request.method == 'POST':
        # Trigger training task (optional)
        train_model_periodically.delay()

        # Load the trained model
        model = joblib.load('trained_model.pkl')

        # Preprocess user input
        user_data = request.form.to_dict()
        preprocessed_data = pd.DataFrame([user_data])
        print("Preprocessed Data:", preprocessed_data)  # Add this line for inspection

        # Make prediction using the loaded model
        prediction = np.expm1(model.predict(preprocessed_data)[0])  # Convert log-sales back to sales

        return render_template('result.html', prediction=prediction)
    else:
        return redirect(url_for('index'))

    
# Result page
@app.route('/result')
def result():
    loading = request.args.get('loading')
    if loading:
        return render_template('result.html', loading=True)
    else:
        # You can modify this section to display the prediction or generate a downloadable submission file
        return render_template('result.html', prediction="Your predicted sales: {{ prediction }}")

if __name__ == '__main__':
    app.run(debug=True)
